/// Copyright (c) 2024 Timothy Landers
/// All rights reserved.  Redistribution and/or modification hereof explicitly prohibited.

//>>> #need libmercury.js
appVersion = 3;

function intToString(n){
  var str="";
  str += typeof n;
  if (typeof n === "number")
  { // 32bit
    str = n.toString(16);
    while (str.length<8) str="0"+str;
    str=" 0x"+str+" ("+(n|0).toString()+")";
  } else {
    // assume 64bit: n[L,H]
    str = int64toHex(n)+" ("+int64toDec(n)+")";
  }
  return str;
}

var ele = null;

// TODO: Remove log() to stand alone library.
var log = (txt)=>
{ var s=document.createElement("span");
  s.append( txt.toString() );
  ele.log.append(s);
}

function clearLog(){
/*
  while (logCount--){
      ele.log.remove(ele.log.lastChild);
  }
*/
// HAX: .remove()-ing all child nodes from the log element prevented appending
//      more nodes, so we rebuild the logging element instead.
  var parent=ele.log.parentElement;
  var lg = document.createElement("div");
  lg.className = "log";
  parent.append(lg);
// parent.remove( ele.log );
// HAX: removing ele.log here prevents new log node from being displayed, so
//      we make the old log element not be rendered.
  ele.log.style.display = "none";
  ele.log = lg;

// HAX: above log() caches 'ele.log' & logs to old element, not new log node.
/*
  log = (txt)=>
  { var s=document.createElement("span");
    s.append(
      txt.toString() //document.createTextNode(txt)
    );
    ele.log.append(s);
  }
*/
// HAX: Above log function is suddenly not needed...
  log( "Cleared Log.\n" );
}

(()=>{ // 'namespace' encapsulation

// Set to "done" when complete; is positive while loading; is negative before loading.
var loading = -1;

// Element ID to DOM node table; initialized from idList
var el = {};
ele=el; //todo:remove

// Space delimited list of ID tags that populate the 'el' table upon init() call.
var idList = (
	"fun log runCode save load selectAll clearLog"
    ).split(' ');

// Finds DOM elements for el when body is loaded.
function loader() {
	var body = document.getElementsByTagName("body")[0];
	if ( !body ) {
		loading = setTimeout( loader, 10 );
		return;
	}
	var index;
	for ( index = idList.length; --index >= 0; ){
		var element = document.getElementById( idList[ index ] );
		if ( element ) el[ idList[ index ] ] = element;
		else {
			// Elements are not loaded yet, try again later.
            loading = setTimeout( loader, 20 );
			return;
		}
	}
	clearTimeout( loading );
	loading = "done";
	init();
}
loader();

// Called when finshed loading DOM elements
function init() {
  onerror=function(e){log('\nerr:'+e+'\n');};

  el.runCode.addEventListener( "click", runCode, true );
// HAX: delete el.fun.readonly; //<- delete don't work.
  el.fun.removeAttribute( "readonly" );
  log("app v" + appVersion+"\n");
  log("lib v" + libVersion+"\n");

  el.testLog.addEventListener('click',(e)=>{log((+new Date())+'\n')},true);

  el.clearLog.addEventListener("click",(e)=>{clearLog()},true);

  el.load.addEventListener("click",(e)=>{loadTxt()},true);
  el.save.addEventListener("click",(e)=>{saveTxt()},true);
  el.selectAll.addEventListener("click",(e)=>{selectAll()},true);
}

function runCode() {
  var result = eval( el.fun.value );

  if ( result === null )
  { log( "Type: null, Value: null\n" );
  } else if ( result != undefined )
  log(
    "Type:" + typeof(result) +
    ", Value:" + result.toString() + "\n"
  );
}

/// Save text buffer.
function saveTxt(){
  localStorage.setItem( "eval", el.fun.value );
}

function loadTxt(){
  alert("No save/load yet");
  el.fun.value = localStorage.getItem( "eval" );
}

function selectAll(){
  el.fun.focus();
  el.fun.select();
}

})(); // end Namespace & invoke it.
