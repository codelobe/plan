libVersion=45;

/********************
FIXEDPOINT 16.16 two's compliment: 16bits of whole &fractional precision.

a signed int32 val == 1/65536th

add1616,sub1616,mul1616,div1616: Same as ordinary int32 OPs (w/ result|0):
a = (a [+-/*] d)|0;
********************/

/// Add w/ 32bit precision.
var add1616 = (d,a)=>{
  return (a+d)|0;
}

/// Subtract d from a w/ 32bit precision.
var sub1616 = (d,a)=>{
  return (a-d)|0;
}

/// Divide a into d parts w/ 32bit precision.
var div1616 = (d,a)=>{
  return (a/d)|0;
}

/// Multiply a by d w/ 32bit precision.
var mul1616 = (d,a)=>{
  return imul32(d,a);
}

/// Round 16.16 to next lower whole nember. 
var floor1616 = (d)=>{
  // and 0xffff0000 rdx rax;
  return 0xffff0000 & d;
}

/// Round 16.16 to next lower whole number.
var ceil1616 = (d)=>{
/*
  add 0xffff rdx;
  and 0xffff0000 rdx rax;
*/
  return (d + 0xffff & 0xffff0000);  
}

/// Round 16.16 > n.5 up to next whole number, or down if <= 0.5.
var round1616 = (d)=>{
/*
  add 0x7fff rdx;
  and 0xffff0000 rdx rax;
*/
  return (d + 0x7fff & 0xffff0000);
}

/// Unary negation (invert sign)
var twos1616 = (d)=>{
  /*
  sub edx 0 eax;
  */
  return (0-d|0);
}

/// Normalize fractional part (get positive n.16).
var frac1616 = (d)=>{
  /*
  and 0xffff rdx rax;
  mov rdx rbx;
  sshr 31 ebx;
  sub edx 0 edx;
  and 0xffff edx;
  and rbx rdx;
  not ebx;
  and rbx rax;
  or rdx rax;
  */
  var a = d & 0xffff;
  var b = d >> 31;
  d = (0-d) & 0xffff;
  return (~b&a) | (b&((0-d) & 0xffff));
}

/// Add d to a with result clamped to +0x7fff.ffff -0x8000.0000
var sat1616 = (d,a)=>{
  a = d+a |0;

}

/// Add d to a with result clamped to +0xffffffff
var sat32 = (d,a)=>{
  /*
  // 64bit H/W impl.
  add rax rdx;
  mov edx eax;   // rax is result if no overflow.
  shr 32 rdx;    // rdx=1 on overflow
  sub edx 0 edx; // 0xffffffff on overflow
  or rdx rax;    // result or (2^32-1) if overflown.

  // 32bit impl w/o flags reg.
  mov eax ecx; and 0xffff ecx; shr 16 eax;
  mov edx ebx; and 0xffff ebx; shr 16 edx;
 
  // add low16 bits
  add ecx ebx; mov ebx ecx;
  shr 16 ebx;
  add ebx eax; //carry
  
  // add high 16 bits
  add eax edx; mov edx eax;
  shl 15 edx;
  sshr 31 edx; // -1 on carry
  
  // compose result
  shl 16 eax;
  mov cx ax;
  or edx eax;
  */
  var c=a&0xffff; a>>>=16;
  var b=d&0xffff; d>>>=16;
  
  // low 16bits
  c+=b;

  // high 16bits
  d+=a; a=d;
  d=(d<<15)>>31;//-1 on carry

  // compose result w/ carry
  return (a<<16)+c|d;
}

/// Subtract d from a with result clamped to zero.
var desat32 = (d,a)=>{
  a = d - a;
  return a & ((-1 ^ a) >> 31);
}

/********************
32bit INTEGER OPs

After each add/sub/mul/div coerce to 32bit int via:(result|0)
Emulates 32bit precision
********************/

var imul32 = Math.imul ? Math.imul : function( d, a ) {
  var d0 = d & 0xffff;
  var d1 = (d|0) >> 16;
  var a0 = a & 0xffff;
  var a1 = (a|0) >> 16;
  return d0*a0 + ( ((d1*a0 + d0*a1) << 16) >>> 0 ) | 0;
}

function idiv32( d, a ) {
  return (a|0)/(d|0) | 0;
	// TODO: implement egyptian division.
}


/********************
64bit INTEGER OPs
********************/

// @return Sum of 64bit params: a[L,H] + b[L,H];
var iadd64 = (a,b)=>{
  var a0=a[0];    // 22bit
  var a1=a[1];    // 22bit
  var a2=a1>>>12; // 20bit
  a1=(a0>>>22|(a1&0xfff)<<10);
  a0&=0x3fffff;

  var b0=b[0];
  var b1=b[1];
  var b2=b1>>>12;
  b1=(b0>>>22|b1<<10)&0x3fffff;
  b0&=0x3fffff;

  var c0=a0+b0;
  var c1=a1+b1+(c0>>22);
  var c2=a2+b2+(c1>>22);

  return [(c0&0x3fffff)|c1<<22,(c1>>>10&0xfff)|c2<<12];
}

/// Subtract b from a: (a-b)
var isub64 = (b,a)=>{
  var b0 = b[0];
  var b1 = b[1];
  var b2=b1>>>12;
  b1=(b0>>>22|b1<<10)&0x3fffff;
  b0&=0x3fffff;
  var a0=a[0];    // 22bit
  var a1=a[1];    // 22bit
  var a2=a1>>>12; // 20bit
  a1=(a0>>>22|(a1&0xfff)<<10);
  a0&=0x3fffff;

  var c0=a0-b0;
  var c1=a1-b1+(c0>>22);
  var c2=a2-b2+(c1>>22);

  return [(c0&0x3fffff)|c1<<22,(c1>>>10&0xfff)|c2<<12];
};

/// @return Product of Multipling 64bit arrays: b*a
var imul64 = (b,a)=>{
  // TODO: Use 5 13bit values; will not overflow and has one less column to sum.
  var b0 = b[0];
  var b4 = b[1];
  var b1=b0>>>13&8191;
  var b2=(b0>>>26|b4<<6)&8191;
  b0 &= 8191;
  var b3=b4>>>7&8191;
  b4 = b4>>>20;

  var a0 = a[0];
  var a4 = a[1];
  var a1=(a0>>>13)&8191;
  var a2=(a0>>>26|a4<<6)&8191;
  a0 &= 8191;
  var a3=(a4>>>7)&8191;
  var a4=a4>>>20;

// 13 bit Base8192 "digit" places:
//       b4    b3    b2    b1    b0
//    x  a4    a3    a2    a1    a0
//      ===============================
//c4-c0: a0*b4 a0*b3 a0*b2 a0*b1 a0*b0
//d4-d1: a1*b3 a1*b2 a1*b1 a1*b0
//e4-e2: a2*b2 a2*b1 a2*b0
//f4-f3: a3*b1 a3*b0 
//   g4: a4*b0
  var c0=a0*b0; var c1=a0*b1; var c2=a0*b2;
  var c3=a0*b3; var c4=a0*b4; var d1=a1*b0;
  var d2=a1*b1; var d3=a1*b2; var d4=a1*b3;
  var e2=a2*b0; var e3=a2*b1; var e4=a2*b2; 
  var f3=a3*b0; var f4=a3*b1; var g4=a4*b0;

  return iadd64
  ( ishl64(  [c4+d4+e4+f4+g4,0], 52 ),
      iadd64
      ( ishl64(   [c3+d3+e3+f3,0], 39 ),
        iadd64
        ( iadd64
          ( ishl64(  [c2+d2+e2,0], 26 ),
            ishl64(     [c1+d1,0], 13 )
          ),
              [c0,0]
  ) ) );
};

/// Divide 64bit: a64/b64
/// @return a64[L,H] divided by b64[L,H] as: [r64[L,H], a64%b64]
var idiv64 = (b64,a64)=>{
  // remainder of prior pass.
  var rem = [0,0];
  // Intermediary Numerator
  var num = [a64[0],a64[1]];
  // result of div: r64[L,H]
  var res = [0,0];

  // TODO: inline ishl64() & loop unroll this.
  for ( var out = 63; out >= 0; --out ){
    var bit = num[1] >>> 31;
    num = ishl64(num,1);
    rem = ior64(
      [bit,0],
      ishl(rem,1)
    );

    // TODO: Tree magic below
    if /*( rem >= b64 )*/
    ( b64[1]>>>0 < rem[1]>>>0
    | ( rem[1]|0===b64[1]|0
      & b64[0]>>>0 < rem[0]>>>0 
    ) ) {
      // rem -= b32;
      rem=isub64(b64,rem);
      var tmp=ishl64([1,0],out);
      //res=ior64(res, tmp);
      res[0] |= tmp[0];
      res[1] |= tmp[1];
    }
  } 
  return [res,rem];
};

/// Divide 64bit by 32bit: a64/b32
/// @return a64[L,H] divided by [b32,0] as: [r64[L,H], a64%b32]
var idiv64by32 = (b32,a64)=>{
  // remainder of prior pass.
  var rem = 0;
  // Intermediary Numerator
  var num = a64;
  // result of div: r64[L,H]
  var res = [0,0];
  // force b32 unsigned & 32bit
  b32=b32>>>0;
  // TODO: inline ishl64() & loop unroll this.
  for ( var out = 63; out >= 0; --out ){
    var bit = num[1] >>> 31;
    num = ishl64(num,1);
    rem = (rem<<1|bit)>>>0;

    // TODO: Tree magic below
    if ( rem >= b32 ) {
      rem = (rem - b32)>>>0;
      var tmp = ishl64([1,0],out);
      //res=ior64(res, tmp);
      res[0]|=tmp[0];
      res[1]|=tmp[1];
    }
  } 
  return [res,rem];
};

/// @return a64[L,H] divided by 10 (ten) as: [r64[L,H], a64%10]
var idiv64byTen = (a64)=>{
  // remainder of prior pass.
  var rem = 0;
  // Intermediary Numerator
  var num = a64;
  // result of div: r64[L,H]
  var res = [0,0];

  // TODO: inline ixxx64() where possible & loop unroll this.
  for ( var out = 63; out >= 0; --out ){
    var bit = num[1] >>> 31;
    num = ishl64(num,1);
    rem = (rem<<1)|bit;

    // TODO: Tree magic below
    if ( rem >= 10 ) {
      rem -= 10;
      res = ior64( res, ishl64([1,0],out) );
    }
  }
  return [res,rem];
};

// TODO: shift(bits,val): result is last operand in .DNA

/// @return a64[L,H] shifted right (lower) by a number of bits.
/// @note no change if bits%64==0
var ishr64 = (a,bits)=>{
  var lo = a[0];
  var hi = a[1];
  bits&=63;
  var m=31-bits>>31;
  var w=~m;
  lo=hi&m|lo&w;hi&=w;
  bits=bits-32&m|bits&w;
  m=0-bits>>31;w=~m;
  lo=(lo>>>bits|hi<<32-bits)&m|lo&w;
  hi=hi>>>bits&m|hi&w;
  return [lo,hi];
};

/// @return a64[L,H] sign extending shifted right (lower) by a number of bits.
/// @note no change if bits%64===0
var issh64 = (a,bits)=>{
  var lo = a[0];
  var hi = a[1];
  bits&=63;
  var m=31-bits>>31;
  var w=~m;
  lo=hi&m|lo&w;
  hi=hi>>31&m|hi&w;
  bits=bits-32&m|bits&w;
  m=0-bits>>31;w=~m;
  lo=(lo>>>bits|hi<<32-bits)&m|lo&w;
  hi=hi>>bits&m|hi&w;
  return [lo,hi];
};

/// @return a[L,H] shifted left (higher) by a number of bits.
/// @note no shift if bits%64 === 0
var ishl64 = (a,bits)=>{
  var lo = a[0];
  var hi = a[1];
  bits&=63;
  var w=31-bits>>31;
  var m=~w;
  hi=w&lo|m&hi;
  bits=w&bits-32|m&bits;lo&=m;
  w=0-bits>>31;m=~w;
  hi=(hi<<bits|lo>>>32-bits)&w|hi&m;
  lo=lo<<bits&w|lo&m;
  return [lo,hi];
};

/// @return Bitwise AND of array parameters.
var iand64 = (a,b)=>{
	return [a[0]&b[0], a[1]&b[1]];
};

/// @return Bitwise OR of array parameters.
var ior64 = (a64,b64)=>{
	return [a64[0]|b64[0], a64[1]|b64[1]];
};

/// @return Bitwise XOR of array parameters.
var ixor64 = (a64,b64)=>{
	return [a64[0]^b64[0], a64[1]^b64[1]];
};

/// @return Bitwise NOT of array parameters.
var inot64 = (a64)=>{
  return [~a64[0], ~a64[1]];
};

/// @return Twos compliment (invert sign): [0,0] - a64[L,H]
var itwos64 = (a)=>{
  var b0 = a[0];
  var b1 = a[1];
  b1=~b1;
  var m=(0-b0|b0)>>31;
  var w=~m;
  b0=~b0;
  b1+=w&1;
  b0=b0+1;
  return [b0,b1];
};

// ###### Text Conversions ######

/// @return (unsigned)Hexadecimal string representation of 64bit array parameter n[L,H]
var int64toHex = (n)=>{
  var s1 = (n[1]>>>0).toString(16).toUpperCase();
  while (s1.length < 8) s1="0"+s1;
  var s0 = (n[0]>>>0).toString(16).toUpperCase();
  while (s0.length < 8) s0="0"+s0;
  return "0x"+s1+s0;
};

/// @return (signed) Decimal string representation of 64bit array parameter n[H,L]
var int64toDec = (n)=>{
  // Negative?
  var neg = n[1]>>>31;
  /// result as returned by idiv64by10
// TODO: inline 2's comp. & optimize out branch.
  var res = [neg ? itwos64(n)
  :n,1]; // must be non zero
  var str = "";
  // cache array access.
  n = res[0];

  do {
    res = idiv64byTen(n);
    str=res[1]+str;
    n = res[0];
  } while (n[0]|n[1]);

  return (neg?"-":"")+str;
}

/// @return 64bit num array for (hex/dec/oct) string.
/// @note Max decimal: 9223372036854775807 (2^63-1)
/// Min decimal: -9223372036854775808 (2^63*-1)
/// Max unsigned decimal 18446744073709551615 will parse returning -1 (2^64-1)
function parseInt64( txt ){
  var st=0; // Marhine State
  var chi=0; // Char Index
  var cn = txt.charCodeAt(chi);
  // Sign is negative?
  var neg = false;
  var res = [0,0];
  // Num Parse State Machine
  while ( st >= 0 ){
  // 'break' increments char; 'continue' keeps current char vars.
    switch ( st ) {
      case 0: // Negative?
        st = 1; // 0_?
        if (cn===0x2D) {
          neg = true;
          break;
        }
        continue;
      case 1: // Hex/Dec? 0_?
        if (cn===0x30) {
          st = 2; // 0x?
          break;
        }
        st = 3; // Dec
        continue;
      case 2: // 0x? Hex/Oct
        if (cn===0x78) {
          st = 4; // Hex
          break;
        }
        st = 5; // Oct
        continue;
      case 3: // Parse Dec
        // Skip comma ,
        if (cn===0x2c) break;
        if ( (cn<0x30) | (cn>0x39) ) {
          // Non [0..9] or ,
          st = -1;
          continue;
        }
// TODO: detect overflow? if res>maxint/10 {err} if res*10+digit < res*10 {err}
        // Mul result by 10 (ten),
        // Add dec digit to result.
        res = iadd64(
          iadd64(
            ishl64( res, 1 ),
            ishl64( res, 3 )
          ),
          [cn-0x30,0]
        );
        break;
      case 4: // Parse Hex
        if
        ( !( ((cn>=0x30) &(cn<=0x39))
          | ((cn>=0x41) & (cn<=0x46))
          | ((cn>=0x61) & (cn<=0x66)) )
        ) {
          // ~! /[0..9A..F]/i
          st = -1;
          continue;
        }
        if ( res[1]&0xf0000000 ) {
          // Overflow
          return neg ? [0,0x80000000] : [-1,0x7fffffff];
        }
        // Dragon!
        cn = (cn | 32) - 87;
        cn += (cn >> 31) & 39;

        // SHL 4 then OR in hex digit.
        res = ior64(
          ishl64(res, 4),
          [cn,0]
        );
        break;

      case 5: // Parse Oct
        if ( (cn<0x30) | (cn>0x37) ) {
          // Non [0..7]
          st = -1;
          continue;
        }
        // Overflow?
        if ( res[1]&0xc0000000 ) {
          return neg ? [0,0x80000000] : [-1,0x7fffffff];
        }

        // Mul result by 8,
        // OR in oct digit.
        res = ior64(
          ishl64( res, 3 ),
          [cn-0x30,0]
        );
        break;
      } // switch
    
    if (++chi >= txt.length) st=-1;
    else {
      cn = txt.charCodeAt(chi);
    }
  } // while

  if (neg) {
    // Negative Overflow?
    if (res[1]>>>31) return [0,0x80000000];
    return itwos64(res);
  }
  return res;
}