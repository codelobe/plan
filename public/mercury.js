/// #need libmercury.js; // 64bit math fn()
uiVersion = 16; // TODO: Remove debug.

onerror = function( err ){
  alert
  (	err.toString()
  );
};

function log(txt){
  var s=document.createElement("span");
  s.appendChild(
    document.createTextNode(txt)
  );
  el.log.appendChild(s);
}

function logInt(n){
  var str="";
  str += typeof n;
  if (typeof n === "number")
  { // 32bit
    str = n.toString(16);
    while (str.length<8) str="0"+str;
    str=" 0x"+str+" ("+(n|0).toString()+")";
  } else {
    // assume 64bit: n[L,H]
    str = int64toHex(n)+" ("+int64toDec(n)+")";
  }
  log(str);
}

// Set to "done" when complete; is positive while loading; is negative before loading.
var loading = -1;

// Element ID to DOM node table; initialized from idList
var el = {};

// Space delimited list of ID tags that populate the 'el' table upon init() call.
var idList = 
( "acc dat hexout decout" +
  " addbtn subbtn mulbtn divbtn expbtn" +
  " shlbtn shrbtn sshbtn andbtn orbtn xorbtn notbtn twosbtn" +
  " hexdat decdat hexacc decacc" +
  " acc64 dat64 hexout64 decout64" +
  " add64btn sub64btn mul64btn div64btn exp64btn" +
  " shl64btn shr64btn ssh64btn and64btn or64btn xor64btn not64btn twos64btn" +
  " hexdat64 decdat64 hexacc64 decacc64" +
  " log"
).split(' ');

// Finds DOM elements for el when body is loaded.
function loader() {
	var body = document.getElementsByTagName("body")[0];
	if ( !body ) {
		loading = setTimeout( loader, 10 );
		return;
	}
	var index;
	for ( index = idList.length; --index >= 0; ){
		var element = document.getElementById( idList[ index ] );
		if ( element ) el[ idList[ index ] ] = element;
		else {
			// Elements are not loaded yet, try again later.
			loading = setTimeout( loader, 20 );
			return;
		}
	}
	clearTimeout( loading );
	loading = "done";
	init();
}
loader();

// Called when finshed loading DOM elements
function init() {
  log( "calc v" + uiVersion );
  log( "\nlibmercury v" + libVersion + '\n');

  el.addbtn.addEventListener( "click", add32, true );
  el.subbtn.addEventListener( "click", sub32, true );
  el.mulbtn.addEventListener( "click", mul32, true );
  el.divbtn.addEventListener( "click", div32, true );
  el.expbtn.addEventListener( "click", exp32, true );

  el.hexacc.addEventListener( "click", hexacc, true );
  el.hexdat.addEventListener( "click", hexdat, true );
  el.decacc.addEventListener( "click", decacc, true );
  el.decdat.addEventListener( "click", decdat, true );

  el.shlbtn.addEventListener( "click", shl32, true );
  el.shrbtn.addEventListener( "click", shr32, true );
  el.sshbtn.addEventListener( "click", ssh32, true );
  el.twosbtn.addEventListener( "click", twos32, true );
  el.andbtn.addEventListener( "click", and32, true );
  el.orbtn.addEventListener( "click", or32, true );
  el.xorbtn.addEventListener( "click", xor32, true );
  el.notbtn.addEventListener( "click", not32, true );	
	
  // 64bit Operator UI
  el.add64btn.addEventListener( "click", add64, true );
  el.sub64btn.addEventListener( "click", sub64, true );
  el.mul64btn.addEventListener( "click", mul64, true );
  el.div64btn.addEventListener( "click", div64, true );
  el.exp64btn.addEventListener( "click", exp64, true );

  // 64bit Bitwise Op UI
  el.or64btn.addEventListener( "click", or64, true );
  el.xor64btn.addEventListener( "click", xor64, true );
  el.and64btn.addEventListener( "click", and64, true );
  el.not64btn.addEventListener( "click", not64, true );
  el.shr64btn.addEventListener( "click", shr64, true );
  el.ssh64btn.addEventListener( "click", ssh64, true );
  el.shl64btn.addEventListener( "click", shl64, true );
  el.twos64btn.addEventListener( "click", twos64, true );

  // 64bit Quick Paste UI
  el.hexdat64.addEventListener( "click",hexdat64, true );
  el.hexacc64.addEventListener( "click", hexacc64, true );
  el.decacc64.addEventListener( "click", decacc64, true );
  el.decdat64.addEventListener( "click", decdat64, true );
}

// Set the 32bit hex & dec fields to an output value. 
function setOutput(out){
  el.decout.value = (out|0);
  var s0=(out>>>0).toString(16).toUpperCase();
  while (s0.length<8)s0="0"+s0;
  el.hexout.value = "0x"+s0;
}

/// Shift Left (Up).
function shl32(){
  var d = parseInt( el.dat.value );
  var a = parseInt( el.acc.value );
  setOutput( a<<d );
}

/// Shift Right (down).
function ssh32(){
  var d = parseInt( el.dat.value );
  var a = parseInt( el.acc.value );
  setOutput( a>>d );
}

/// Unsigned Shift Right (down).
function shr32(){
  var d = parseInt( el.dat.value );
  var a = parseInt( el.acc.value );
  setOutput( a>>>d );
}

function and32(){
  var d = parseInt( el.dat.value );
  var a = parseInt( el.acc.value );
  setOutput( a&d );
}

function or32(){
  var d = parseInt( el.dat.value );
  var a = parseInt( el.acc.value );
  setOutput( a|d );	
}

function xor32(){
  var d = parseInt( el.dat.value );
  var a = parseInt( el.acc.value );
  setOutput( a^d );	
}

function not32(){
  var a = parseInt( el.acc.value );
  setOutput( ~a );
}

function twos32(){
    var a = parseInt( el.acc.value );
  setOutput( -a );
}

// Add Dat to Acc
function add32(){
  var d = parseInt( el.dat.value );
  var a = parseInt( el.acc.value );
  setOutput( a+d | 0 );
}

// Subtract Dat from Acc
function sub32(){
  var d = parseInt( el.dat.value );
  var a = parseInt( el.acc.value );
  setOutput( a-d | 0 );
}

function mul32(){
  var d = parseInt( el.dat.value );
  var a = parseInt( el.acc.value );
  setOutput( imul32( d, a ) );
}

function div32(){
  var d = parseInt( el.dat.value );
  var a = parseInt( el.acc.value );
  setOutput( idiv32(d, a) );
}

function exp32(){
  var d = parseInt( el.dat.value );
  var a = parseInt( el.acc.value );
  setOutput( a**d );
}

// 64bit UI buttons follow.

/// Set output fields from 64bit num array result.
function setOutput64( n ){
   el.hexout64.value = int64toHex(n);
   el.decout64.value = int64toDec(n);
}

// Add Dat64 to Acc64
function add64(){
  var d = parseInt64( el.dat64.value );
  var a = parseInt64( el.acc64.value );
  setOutput64( iadd64(a,d) );
}

function sub64(){
  var d = parseInt64( el.dat64.value );
  var a = parseInt64( el.acc64.value );
  setOutput64( isub64(d,a) );
}

function mul64(){
  var d = parseInt64( el.dat64.value );
  var a = parseInt64( el.acc64.value );
  setOutput64( imul64(d,a) );
}

function div64(){
  var d = parseInt64( el.dat64.value );
  var a = parseInt64( el.acc64.value );
  if // d[] could be a 32bit value. 
  ( ( d[1] === 0
    | ( d[1]|0 === -1 )
    ) & ((d[0])>>>31 === d[1]&1)
  ) {
    // Divide by 32bit
    a = idiv64by32(d[0],a);
    log("a[0]:"+a[0].toString()+"\n");
    log("a[1]:"+a[1].toString()+"\n");
    setOutput64( a[0] );
    setOutput( a[1] ); // mod
  } else {
    // Full 64bit divide
    a = idiv64( d, a );
    setOutput64( a[0] );
    setOutput( a[1] ); // mod
  }
}

function exp64(){
  // TODO: Modular exp
  var d = parseInt64( el.dat64.value );
  var a = parseInt64( el.acc64.value );
  
  var res = [0,0];

 for ( var out = 63; out >= 0; --out ){
    res = ior64( res, ishl64([1,0],out) );
  }
  setOutput64(res);
}

function shr64(){
  var d = parseInt64( el.dat64.value );
  var a = parseInt64( el.acc64.value );
  setOutput64( ishr64(a,d[0]) );
}

function shl64(){
  var d = parseInt64( el.dat64.value );
  var a = parseInt64( el.acc64.value );
  setOutput64( ishl64(a,d[0]) );
}

function ssh64(){
  var d = parseInt64( el.dat64.value );
  var a = parseInt64( el.acc64.value );
  setOutput64( issh64(a,d[0]) );
}

function or64(){
  var d = parseInt64( el.dat64.value );
  var a = parseInt64( el.acc64.value );
  setOutput64( ior64(d,a) );
}

function xor64(){
  var d = parseInt64( el.dat64.value );
  var a = parseInt64( el.acc64.value );
  setOutput64( ixor64(d,a) );
}

function not64(){
  var a = parseInt64( el.acc64.value );
  setOutput64( inot64(a) );
}

function and64(){
  var d = parseInt64( el.dat64.value );
  var a = parseInt64( el.acc64.value );
  setOutput64( iand64(d,a) );
}

function twos64(){
  var a = parseInt64( el.acc64.value );
  setOutput64( itwos64(a) );
}
// UI quick cpy/paste buttons follow.

function hexacc() {
	el.acc.value = el.hexout.value;
}

function hexdat(){
	el.dat.value = el.hexout.value;
}

function decacc() {
	el.acc.value = el.decout.value;
}

function decdat(){
	el.dat.value = el.decout.value;
}

function hexacc64() {
	el.acc64.value = el.hexout64.value;
}

function hexdat64(){
	el.dat64.value = el.hexout64.value;
}

function decacc64() {
	el.acc64.value = el.decout64.value;
}

function decdat64(){
	el.dat64.value = el.decout64.value;
}
