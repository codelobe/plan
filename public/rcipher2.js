/*
	Retrograde Cipher: A Pre-Shared Key encryption tool.
	Copyright 2011,2013 Timothy Landers
	<vortexcortex@project-retrograde.com>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

/// RCipher - ASM.js implementation of the Retrograde Cipher transcoder.
///
/// @return		RetroCipher ASM.js instance.
///
function RCipher( std, foreign, heap )
{    "use asm";

/*	Memory Map

	  Def. Range | Length  | Var | Description
	-------------|---------|-----|------------------
	    0 -  316 |  320 B  | off | Accumulator Hash
	-------------|---------|-----|------------------
	  320 -  636 |  320 B  | alt | Block Cipher Hash
	-------------|---------|-----|------------------
	  640 -  892 |  256 B  | enc | Encipher Table
	-------------|---------|-----|------------------
	  896 - 1148 |  256 B  | dec | Decipher Table
	-------------|---------|-----|------------------
	 1152 - ???? | >=2.7KB | usr | User I/O Data
	-------------'---------'-----'------------------
*/
	var _4 = new std.Uint32Array( heap );
	var _1 = new std.Uint8Array( heap );

	// Hashing Vars

	var h0 = 0x67452301;			// SHA1 Initialization vector.
	var h1 = 0xefcdab89;
	var h2 = 0x98badcfe;
	var h3 = 0x10325476;
	var h4 = 0xc3d2e1f0;

	var byteCount = 0;				// Total length of message in bytes.
	var blockByte = 0;				// Current byte within the block.
	var off = 0;					// Offset of this digest in the heap.

	// RCipher Vars.

	var alt = 320;					// Second offset of the digest, used when ciphering.
	var enc = 640;					// Offset of encode substitution cipher table.
	var dec = 896;					// Offest of decode substitution cipher table.
	var usr = 1152;					// Offset to the user input data. 1.25KB
	var wrd = 0;					// Current word number of the digest.
	var sft = 0;					// Bits to shift wrd right to get the current byte.
	var bits = 0;					// Current word's bits.

	// Hashing Functions.

	/// Sets the offset of the memory used by the DigestSHA1 entity.
	/// The DigestSHA1 needs 320 bytes of memory to operate on.
	function offset( addr )
	{	addr = addr|0;
		off = addr;
		return;
	}

	/// Prepares the DigestSHA1 entity to consume bytes.  Call before using add().
	function reset()
	{	var i = 0;
		h0 = 0x67452301;
		h1 = 0xefcdab89;
		h2 = 0x98badcfe;
		h3 = 0x10325476;
		h4 = 0xc3d2e1f0;
		blockByte = 0;
		byteCount = 0;
		for ( ; (i|0) < 64; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = 0;
		return;
	}

	/// Stores the DigestSHA1 state into the first 92 bytes after the address.
	function store()
	{	_4[ (off + 64) >> 2 ] = byteCount|0;
		_4[ (off + 68) >> 2 ] = h0|0;
		_4[ (off + 72) >> 2 ] = h1|0;
		_4[ (off + 76) >> 2 ] = h2|0;
		_4[ (off + 80) >> 2 ] = h3|0;
		_4[ (off + 88) >> 2 ] = h4|0;
		return;
	}

	/// Loads the DigestSHA1 state from the 92 bytes at the address specified.
	function load( addr )
	{	addr = addr|0;
		var i = 0;
		for ( ; (i|0) < 64; i = ( i + 4 )|0 ) _4[ (off + i) >> 2 ] = _4[ (addr + i) >> 2 ] | 0;
		byteCount = _4[ (addr + 64 ) >> 2 ]|0;
		blockByte = byteCount & 0x3f;
		h0 = _4[ (addr + 68) >> 2 ]|0;
		h1 = _4[ (addr + 72) >> 2 ]|0;
		h2 = _4[ (addr + 76) >> 2 ]|0;
		h3 = _4[ (addr + 80) >> 2 ]|0;
		h4 = _4[ (addr + 88) >> 2 ]|0;
		return;
	}

	/// Adds a number of bytes to the message digest.
	function add( data, len )
	{	data = data|0;
		len = len|0;
		var end = 0;
		var pos = 0;

		// Process the 512 bit block if it's full.
		if ( (blockByte|0) >= 64 ) processChunk();
		end = ( data + len )|0;

		// Process each character of the input string.
		while ( (data|0) < (end|0) )
		{	// Fill the first 512 bits of the block.
			pos = ( off + blockByte )|0;
			_4[ pos >> 2 ] = _4[ pos >> 2 ] | ( (_1[ data ]|0) << (24 - ((blockByte & 3) << 3)) );
			blockByte = ( blockByte + 1 )|0;
			data = ( data + 1 )|0;
			// Process the 512 bit chunk if enough bits have been added.
			if ( (blockByte|0) >= 64 ) processChunk();
		}

		byteCount = ( byteCount + len )|0;
		return;
	}

	/// Computes the 20 byte (160 bit) message digest and stores it at the offset of this DigestSHA1 entity.
	function digest()
	{	var len = 0;
		var pos = 0;

		if ( (blockByte|0) >= 64 ) processChunk();

		// Get the length in bits and write the first padding byte.
		len = byteCount << 3;
		pos = ( off + 64 )|0;
		_1[ pos ] = 0x80;
		add( pos, 1 );

		// Add remaining zero padding if needed (First 512 bytes are already zero padded).
		if ( (blockByte|0) > 56 ) processChunk();

		// Store the length in bits at the 14th and 15th words, then process the final chunk.
		_4[ (off + 56) >> 2 ] = byteCount >>> 29; // TODO: Add support for 64bit length.
		_4[ (off + 60) >> 2 ] = len & 0xffffffff;
		processChunk();

		// Store the hash vars.
		_4[  off        >> 2 ] = h0|0;
		_4[ (off +  4 ) >> 2 ] = h1|0;
		_4[ (off +  8 ) >> 2 ] = h2|0;
		_4[ (off + 12 ) >> 2 ] = h3|0;
		_4[ (off + 16 ) >> 2 ] = h4|0;

		return;
	}

	/// Applies key stretching iterations. After .add() and .digest() have been called, call this to stretch the key a number of times.
	function stretch( iterations )
	{	iterations = iterations|0;
		var i = 0;

		// Stretch the key.
		while ( (iterations = (iterations - 1)|0) >= 0 )
		{	byteCount = 20;
			blockByte = 20;
			h0 = 0x67452301;
			h1 = 0xefcdab89;
			h2 = 0x98badcfe;
			h3 = 0x10325476;
			h4 = 0xc3d2e1f0;
			for ( i = 20; (i|0) < 64; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = 0;
			digest();
		}
		byteCount = 20;
		blockByte = 20;
		h0 = 0x67452301;
		h1 = 0xefcdab89;
		h2 = 0x98badcfe;
		h3 = 0x10325476;
		h4 = 0xc3d2e1f0;
		for ( i = 20; (i|0) < 64; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = 0;

		return;
	}

	// Processes the first 512 bits of the block.
	function processChunk()
	{	var a = 0;
		var b = 0;
		var c = 0;
		var d = 0;
		var e = 0;
		var f = 0;
		var g = 0;

		var i = 64;

		// Initialize the hash-round.
		a = h0;
		b = h1;
		c = h2;
		d = h3;
		e = h4;

		// Expand the first 512 bits into 2560 bits.
		for ( ; (i|0) < 320; i = (i + 4)|0 )
		{	f = _4[ (off + i - 12) >> 2 ] ^
				_4[ (off + i - 32) >> 2 ] ^
				_4[ (off + i - 56) >> 2 ] ^
				_4[ (off + i - 64) >> 2 ];
			_4[ (off + i) >> 2 ] = ( f >>> 31 ) | ( f << 1 );
		}

		// Process the first 20 rounds.
		for ( i = 0; (i|0) < 80; i = (i + 4)|0 )
		{	f = (b & c) | ((~b) & d);
			g = ( ((a >>> 27) | (a << 5)) + f + e + 0x5a827999 + (_4[ (off + i) >> 2 ]|0) )|0;
			e = d;
			d = c;
			c = ( b >>> 2 ) | ( b << 30 );
			b = a;
			a = g;
		}

		// Process rounds 20-40.
		for ( i = 80; (i|0) < 160; i = (i + 4)|0 )
		{	f = b ^ c ^ d;
			g = ( ((a >>> 27) | (a << 5)) + f + e + 0x6ed9eba1 + (_4[ (off + i) >> 2 ]|0) )|0;
			e = d;
			d = c;
			c = ( b >>> 2 ) | ( b << 30 );
			b = a;
			a = g;
		}

		// Process rounds 40-60.
		for ( i = 160; (i|0) < 240; i = (i + 4)|0 )
		{	f = (b & c) | (b & d) | (c & d);
			g = ( ((a >>> 27) | (a << 5)) + f + e + 0x8f1bbcdc + (_4[ (off + i) >> 2 ]|0) )|0;
			e = d;
			d = c;
			c = ( b >>> 2 ) | ( b << 30 );
			b = a;
			a = g;
		}

		// Process rounds 60-80.
		for ( i = 240; (i|0) < 320; i = (i + 4)|0 )
		{	f = b ^ c ^ d;
			g = ( ((a >>> 27) | (a << 5)) + f + e + 0xca62c1d6 + (_4[ (off + i) >> 2 ]|0) )|0;
			e = d;
			d = c;
			c = ( b >>> 2 ) | ( b << 30 );
			b = a;
			a = g;
		}

		// Set the new hash value.
		h0 = ( h0 + a )|0;
		h1 = ( h1 + b )|0;
		h2 = ( h2 + c )|0;
		h3 = ( h3 + d )|0;
		h4 = ( h4 + e )|0;

		// Prepare the block for another chunk.
		blockByte = 0;
		for ( i = 0; (i|0) < 64; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = 0;

		return;
	}

	// RCipher Functions

	/// Performs HMAC on a key and message, leaves the hash setup for digest().
	function hmac( keyOff, keyLen, msgOff, msgLen )
	{	keyOff = keyOff|0;
		keyLen = keyLen|0;
		msgOff = msgOff|0;
		msgLen = msgLen|0;

		var i = 0;
		var dig0 = 0;
		var dig1 = 0;
		var dig2 = 0;
		var dig3 = 0;
		var dig4 = 0;

		reset();

		if ( (keyLen|0) > 64 )
		{	// Hash the key if it's too long.
			add( keyOff, keyLen );
			digest();

			// Copy the hash vars into the key and create opad.
			_4[  keyOff       >> 2 ] = h0 ^ 0x5c5c5c5c;
			_4[ (keyOff +  4) >> 2 ] = h1 ^ 0x5c5c5c5c;
			_4[ (keyOff +  8) >> 2 ] = h2 ^ 0x5c5c5c5c;
			_4[ (keyOff + 12) >> 2 ] = h3 ^ 0x5c5c5c5c;
			_4[ (keyOff + 16) >> 2 ] = h4 ^ 0x5c5c5c5c;

			// Setup the hash as ipad.
			// keyLen = 20;
			h0 = 0x67452301;
			h1 = 0xefcdab89;
			h2 = 0x98badcfe;
			h3 = 0x10325476;
			h4 = 0xc3d2e1f0;
			for ( i = 0; (i|0) < 20; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = _4[ (off + i) >> 2 ] ^ 0x36363636;
			for ( i = 20; (i|0) < 64; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = 0x36363636;
		}
		else
		{	// Add key to the hash and create ipad and opad.
			add( keyOff, keyLen );
			for ( i = 0; (i|0) < 64; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = _4[ (off + i) >> 2 ] ^ 0x36363636;
		}

		byteCount = 64;
		blockByte = 64;

		// Add the message to the hash.
		add( msgOff, msgLen );
		digest();

		// Save the digest of: hash( (ipad ^ key) + message )
		dig0 = h0;
		dig1 = h1;
		dig2 = h2;
		dig3 = h3;
		dig4 = h4;

		// Reset the hash and fill with opad.
		h0 = 0x67452301;
		h1 = 0xefcdab89;
		h2 = 0x98badcfe;
		h3 = 0x10325476;
		h4 = 0xc3d2e1f0;
		if ( (keyLen|0) > 64 )
		{	for ( i = 0; (i|0) < 20; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = _4[ (keyOff + i) >> 2 ];
			for ( ; (i|0) < 64; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = 0x5c5c5c5c;
		}
		else
		{	for ( i = 0; (i|0) < 64; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = 0;
			byteCount = 0;
			blockByte = 0;
			add( keyOff, keyLen );
			for ( i = 0; (i|0) < 64; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = _4[ (off + i) >> 2 ] ^ 0x5c5c5c5c;
		}

		// Perform a hash round on the full buffer, then add the digest.
		byteCount = 64;
		blockByte = 64;
		processChunk();
		byteCount = 84;
		blockByte = 20;
		_4[  off       >> 2 ] = dig0;
		_4[ (off +  4) >> 2 ] = dig1;
		_4[ (off +  8) >> 2 ] = dig2;
		_4[ (off + 12) >> 2 ] = dig3;
		_4[ (off + 16) >> 2 ] = dig4;

		return;
	}	// hmac()

	/// Gets or sets the current cipher IO base position.
	/// Pass a positive or zero value to set the new IO pos,
	/// Pass a negative value to leave the value unchanged.
	/// @return		current IO offset within the heap, or -1 on error.
	function cipherIO( pos )
	{	pos = pos|0;
		if ( (pos|0) >= 0 )
		{	if ( (pos|0) >= 1152 ) usr = pos;
			else pos = -1;
		}
		else pos = usr;
		return pos|0;
	}

	/// Keys the RetroCipher to the specified data.
	function key( keyOff, keyLen, iniOff, iniLen )
	{	keyOff = keyOff|0;
		keyLen = keyLen|0;
		iniOff = iniOff|0;
		iniLen = iniLen|0;

		var i = 0;
		var k = 0;
		var byte = 0;
		var pos = 0;
		var bits = 0;

		wrd = -4;
		sft = -1;

		// HMAC and keystretch to init the accumulation hash.
		off = 0;
		hmac( keyOff, keyLen, iniOff, iniLen );
		digest();
		stretch( 1024 );

		// Init the substitution cipher.
		k = ( enc + 255 )|0;
		for ( i = 255; (i|0) >= 0; i = (i - 1)|0 )
		{	_1[ k ] = i;
			k = ( k - 1 )|0;
		}

		// Scramble the subsitution cipher five times; 1280 byte swaps.
		for ( k = 5; (k|0) > 0; k = (k - 1)|0 )
		{	for ( i = 0; (i|0) < 256; i = (i + 1)|0 )
			{	// Swap two byte positions.
				if ( (sft|0) < 0 )
				{	sft = 24;
					wrd = ( wrd + 4 )|0;
					if ( (wrd|0) > 16 )
					{	// Stretch to generate 5 new words of cipher stream.
						stretch(1);
						wrd = 0;
					}
					bits = _4[ (off + wrd) >> 2 ]|0;
				}
				pos = ( bits >> sft ) & 255;
				sft = ( sft - 8 )|0;

				// Swap the current location with the specified location.
				byte = _1[ (enc + i)|0 ]|0;
				_1[ (enc + i)|0 ] = _1[ (enc + pos)| 0 ];
				_1[ (enc + pos)|0 ] = byte;
			}
		}

		// Used up all the bytes, so produce another hash to kick things off.
		stretch( 1 );

		// Reset the hash, leaving the current digest in the accumulator.
		h0 = 0x67452301;
		h1 = 0xefcdab89;
		h2 = 0x98badcfe;
		h3 = 0x10325476;
		h4 = 0xc3d2e1f0;
		byteCount = 20;
		blockByte = 20;
		for ( i = 20; (i|0) < 64; i = (i + 4)|0 ) _4[ (off + i) >> 2 ] = 0;

		// Initialize the reverse substitution cipher.
		for ( i = 0; (i|0) < 256; i = (i + 1)|0 )
		{	_1[ (dec + (_1[ (enc + i)|0 ]|0))|0 ] = i;
		}

		// Set the cipher byte pos to require more bits.
		wrd = 16;
		sft = 0;
		return;
	}

	/// Encodes a range of bytes with the RCipher.
	function encode( len )
	{	len = len|0;

		var pos = 0;
		var end = 0;
		var byte = 0;

		pos = usr;
		end = (usr + len)|0;

		while ( (pos|0) < (end|0) )
		{	if ( (sft|0) == 0 )
			{	sft = 32;
				wrd = ( wrd + 4 )|0;
				if ( (wrd|0) > 16 )
				{	// Clone the hash and digest it to generate more cipher stream.
					store();
					off = alt;
					load( 0 );
					digest();
					wrd = 0;
					off = 0;
					load( 0 );
				}
				bits = _4[ (alt + wrd) >> 2 ]|0;
			}
			sft = ( sft - 8 )|0;
			byte = ( bits >> sft ) & 255;

			// Accumulate plaintext prior to ciphering.
			add( pos, 1 );

			// Encode the byte.
			_1[ pos ] = _1[ (enc + (_1[ pos ] & 255))|0 ] ^ byte;
			pos = ( pos + 1 )|0;
		}
		return;
	}

	/// Decodes a range of bytes with the RCipher.
	function decode( len )
	{	len = len|0;

		var pos = 0;
		var end = 0;
		var byte = 0;

		pos = usr;
		end = (usr + len)|0;

		while ( (pos|0) < (end|0) )
		{	if ( (sft|0) == 0 )
			{	sft = 32;
				wrd = ( wrd + 4 )|0;
				if ( (wrd|0) > 16 )
				{	// Clone the hash and digest it to generate more cipher stream.
					store();
					off = alt;
					load( 0 );
					digest();
					wrd = 0;
					off = 0;
					load( 0 );
				}
				bits = _4[ (alt + wrd) >> 2 ]|0;
			}
			sft = ( sft - 8 )|0;
			byte = ( bits >> sft ) & 255;

			// Decode the byte.
			_1[ pos ] = _1[ (dec + (_1[ pos ] ^ byte))|0 ];

			// Accumulate decoded plaintext.
			add( pos, 1 );

			pos = ( pos + 1 )|0;
		}
		return;
	}

	return {
		offset: offset,
		reset: reset,
		add: add,
		digest: digest,
		stretch: stretch,
		hmac: hmac,
		store: store,
		load: load,

		key: key,
		encode: encode,
		decode: decode,
		cipherIO: cipherIO
	};
}	// RCipher

/// JS Interface to Retrograde Cipher ASM.js instance.
function RetroCipher()
{	var memSize = 4096 * ( 1 << 8 );			// Only change '8'; Must be a power of 2 multiple of 4096.
	var memory = new ArrayBuffer( memSize );	// Create a 1 megabyte array buffer for the program heap.
	var _1 = new Uint8Array( memory );
	var _4 = new Uint32Array( memory );

	var hash = RCipher( window, {}, memory );
	hash.offset( 0 );
	hash.reset();

	var off = 0;
	var pos = 1152;

	/// Adds a text string with each character value being one byte of the message.
	/// @note Convert to UTF-8 first: str = unescape( encodeURIComponent( str ) )
	this.add = function( str )
	{	var len = str.length;
		var i = 0;
		// TODO Break this into several calls if needed.
		while ( i < len ) _1[ pos + i ] = str.charCodeAt( i++ );
		hash.add( pos, len );
	}

	/// Adds a hex string with two characters being one byte of the message.
	/// @note Partial bytes are not allowed, the hex string must have an even length.
	this.addHex = function( hexStr )
	{	var len = hexStr.length >> 1;
		var i = 0;
		// TODO Break this into several calls if needed.
		while ( i < len )
		{	var hi = hexStr.charCodeAt( (i << 1) );
			if ( hi > 0x39 ) hi = ((hi | 0x20) - 87) << 4;
			else hi = (hi & 15) << 4;
			var lo = hexStr.charCodeAt( (i << 1) + 1 );
			if ( lo > 0x39 ) lo = ((lo | 0x20) - 87);
			else lo &= 15;
			_1[ pos + i++ ] = hi | lo;
		}
		hash.add( pos, len );
	}

	/// Adds 32bit integers, each element being four bytes of the message.
	this.addInt = function( uint32array, elements, offset )
	{	elements = elements|0;
		offset = offset|0;
		var i = 0;
		// TODO Break this into several calls if needed.
		while ( i < elements )
		{	_1[ pos + i ] = uint32array[ offset + i++ ];
		}
		hash.add( pos, elements << 2 );
	}

	/// Set the hash offset.
	this.offset = function( addr )
	{	hash.offset( addr );
		off = addr;
	}

	/// @return		string with each character value being one byte of the message.
	/// @note		Convert back from UTF-8: str = decodeURIComponent( escape( str ) )
	this.digest = function()
	{	hash.digest();
		var str = "";
		for ( var w = 0; w < 5; ++w )
		{	var n = _4[ (off >> 2) + w ];
			for ( var i = 24; i >= 0; i -= 8 ) str += String.fromCharCode( ( n >> i ) & 255 );
		}
		hash.reset();
		return str;
	}

	/// @return		A string with two hex digits for each byte of the message.
	this.digestHex = function()
	{	hash.digest();
		var str = "";
		for ( var w = 0; w < 5; ++w )
		{	var n = _4[ (off >> 2) + w ];
			for ( var i = 28; i >= 0; i -= 4 ) str += PUB64[ ( n >> i ) & 15 ];
		}
		hash.reset();
		return str;
	}

	/// @return		Copies the 160bit digest (five 32bit integers) into the array at the offset.
	/// @note		Read digest from [offset] to [offset+4], from least to most significant byte.
	this.digestInt = function( uint32array, offset )
	{	hash.digest();
		offset = offset|0;
		var str = "";
		for ( var w = 0; w < 5; ++w )
		{	uint32array[ w + offset ] = _4[ (off >> 2) + w ];
		}
		hash.reset();
	}

	/// Performs key stretching iterations.  Must be called after .add().
	/// Call .digest() after stretch() to get the desired digest type.
	this.stretch = function( iterations )
	{	hash.digest();
		hash.stretch( iterations - 1 );
	}

	/// Hashed Message Authentication Code
	/// @return	a SHA-1 HMAC.
	this.hmac = function( key, message )
	{	var len = key.length;
		var i = 0;
		for ( ; i < len; ) _1[ pos + i ] = key.charCodeAt( i++ );
		var mpos = pos + i;
		len = message.length;
		for ( i = 0; i < len; ) _1[ mpos + i ] = message.charCodeAt( i++ );
		hash.hmac( pos, key.length, mpos, message.length );
		return this.digest();
	}

	/// Hashed Message Authentication Code
	/// @return	a SHA-1 HMAC in ASCII Hexadecimal.
	this.hmacHex = function( key, message )
	{	var len = key.length;
		var i = 0;
		for ( ; i < len; ) _1[ pos + i ] = key.charCodeAt( i++ );
		var mpos = pos + i;
		len = message.length;
		for ( i = 0; i < len; ) _1[ mpos + i ] = message.charCodeAt( i++ );
		hash.hmac( pos, key.length, mpos, message.length );
		return this.digestHex();
	}

	// Direct ASM.js reference.
	this.reset = hash.reset;

	// Cipher Functions.

	/// Keys the cipher with byte strings prior to encoding or decoding.
	this.key = function( key, initVec )
	{	var len = key.length;
		var i = 0;
		for ( ; i < len; ) _1[ pos + i ] = key.charCodeAt( i++ );
		var vpos = pos + i;
		len = initVec.length;
		for ( i = 0; i < len; ) _1[ vpos + i ] = initVec.charCodeAt( i++ );
		hash.key( pos, key.length, vpos, initVec.length );
		return;
	}

	/// Encodes a string of data.
	/// @return		Encoded data.
	this.encode = function( data )
	{	var len = data.length;
		var i = 0;
		for ( ; i < len; ) _1[ pos + i ] = data.charCodeAt( i++ );
		hash.encode( len );
		data = '';
		for ( i = 0; i < len; ) data += String.fromCharCode( _1[ pos + i++ ] );
		return data;
	}

	/// Decodes a string of data.
	/// @return		Decoded data.
	this.decode = function( data )
	{	var len = data.length;
		var i = 0;
		for ( ; i < len; ) _1[ pos + i ] = data.charCodeAt( i++ );
		hash.decode( len );
		data = '';
		for ( i = 0; i < len; ) data += String.fromCharCode( _1[ pos + i++ ] );
		return data;
	}
}

/// @return		String normalized into UTF-8 character encoding.
function strToUTF8( s )
{	return unescape( encodeURIComponent( s ) );
}

/// @return		UTF-8 characters as a string in the browser's char encoding.
function UTF8ToStr( s )
{	return decodeURIComponent( escape( s ) );
}

// Parameter and URL safe Base64 character set array (in UTF-8).
var PUB64 = strToUTF8( "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ._-" ).split("");

// UTF-8 codepoint constants for PUB64 string encoding / decoding.
var CHR_0 = 0x30;
var CHR_9 = 0x39;
var CHR_a = 0x61;
var CHR_z = 0x7A;
var CHR_A = 0x41;
var CHR_Z = 0x5A;
var CHR_  = 0x2E; // dot
var CHR__ = 0x5F; // underscore
var CHRsp = 0x2D; // separator

// DOM elements.
var txt = null;
var key = null;
var chk = null;
var vec = null;
var bar = null;

// Misc vars.
var cipher = null;
var showKey = false;
var ivLen = 44;
var iv = "";
var ivPrior = "";
var interval = null;
var notified = false;
var target = 400;
var count = 0;
var lastSum = 0;

/// @return 	Text string as a parameter and URL safe base 64 string.
function strToPUB64( s )
{	var len = s.length;
	var b = "";
	var i = 0;
	var buffer = 0;
	var bits = 0;
	while ( i < len )
	{	buffer |= (s.charCodeAt( i++ ) & 0xff) << ( 24 - bits );
		bits += 8;
		while ( bits >= 6 )
		{	b += PUB64[ (buffer >> 26) & 63 ];
			bits -= 6;
			buffer <<= 6;
		}
	}
	if ( bits > 0 ) b += PUB64[ (buffer >> 26) & 63 ];
	return b;
}

/// @return		PUB64 string converted into the browser's string encoding.
function PUB64ToStr( b )
{	var len = b.length;
	var s = "";
	var i = 0;
	var buffer = 0;
	var bits = 0;
	while ( i < len )
	{	var ch = b.charCodeAt( i++ ) & 0xff;
		if ( (ch >= CHR_0) && (ch <= CHR_9) ) ch -= CHR_0;
		else if ( (ch >= CHR_a) && (ch <= CHR_z) ) ch = ( ch - CHR_a ) + 10;
		else if ( (ch >= CHR_A) && (ch <= CHR_Z) ) ch = ( ch - CHR_A ) + 36;
		else if ( ch == CHR_ ) ch = 62;
		else if ( ch == CHR__ ) ch = 63;
		else continue;
		buffer |= ch << ( 26 - bits );
		bits += 6;
		while ( bits >= 8 )
		{	s += String.fromCharCode( (buffer >> 24) & 255 );
			bits -= 8;
			buffer <<= 8;
		}
	}
	return s;
}

/// Toggles the visibility of the secret.
function swap()
{	key.type = ( showKey = !showKey ) ? "text" : "password";
}

/// Generates entropy from mouse coordinates.
function mouseVec( e )
{	var sum = e.clientX + e.clientY;
	if ( Math.abs( lastSum - sum ) < 19 ) return;
	lastSum = sum;
	addIV( sum );
	progress( 1 );
}

/// Generates entropy from key press and release.
function keyVec( e )
{	if ( lastSum == e.keyCode ) return;
	addIV( lastSum = e.keyCode );
	var ms = +(new Date())
	addIV( ms );
	addIV( ms >> 4 );
	addIV( ms >> 8 );
	progress( 3 );
}

/// Updates the entropy progress bar.
function progress( amount )
{	if ( (count += amount) > target ) count = target;
	var ratio = count / target;
	bar.style.width = "" + ( ratio * 100 ) + "%";

	var red = 255;
	var green = 0;
	var blue = 0;

	if ( ratio < 0.75 )
	{	ratio *= 1.333333333;
		red = 255;
		green = 180 * ratio;
		blue = 0;
	}
	else if ( ratio == 1 )
	{
		red = 32;
		green = 255;
		blue = 40;
	}
	else
	{	ratio = (ratio - 0.75) * 4;
		red = 255 - 255 * ratio;
		green = 180;
		blue = 32 * ratio;
	}
	bar.style.backgroundColor = "rgb(" + Math.floor( red ) + "," + Math.floor( green ) + "," + Math.floor( blue ) + ")";
}

/// Updates the initialization vector.
function addIV( dat )
{	if ( interval != null ) return;
	iv += PUB64[ dat & 15 ];
	if ( iv.length >= (ivLen << 1) )
	{	cipher.reset();
		cipher.addHex( ivPrior + iv );
		ivPrior = cipher.digestHex();
		iv = "";
	}
	cipher.reset();
	cipher.addHex( ivPrior + iv );
	while ( vec.firstChild ) vec.removeChild( vec.firstChild );
	vec.appendChild( document.createTextNode( strToPUB64( cipher.digest() ) ) );
	return 0;
}

/// Encodes text.
function encipher()
{	cipher.reset();
	cipher.addHex( ivPrior + iv );
	var initVec = cipher.digest();
	cipher.reset();
	cipher.key( strToUTF8( key.value ), initVec );
	var str = "~" + strToPUB64( initVec + cipher.encode( strToUTF8( txt.value ) ) );
	var ret = "";
	while ( str.length > 72 )
	{	ret += str.substr( 0, 72 ) + "\n";
		str = str.substr( 72 );
	}
	txt.value = ret + str;
	count = 0;
	progress( 0 );
}

/// Decodes text.
function decipher()
{	var initVec = PUB64ToStr( strToUTF8( txt.value ) );
	if ( initVec.length < 20 ) return;
	var str = initVec.substr( 20 );
	var initVec = initVec.substr( 0, 20 );
	cipher.reset();
	try
	{	cipher.key( strToUTF8( key.value ), initVec );
		txt.value = UTF8ToStr( cipher.decode( str ) );
	}
	catch( ex )
	{	str = txt.value;
		if
		(	!notified
			&& str.charCodeAt( 27 ) == 0x2E
			&& str.charCodeAt( 55 ) == 0x2E
			&& str.lastIndexOf( '.' ) == 55
		)
		{	notified = true;
			alert
			(	"The message looks like it might have been encoded\n"+
				"with a previous version of the Retrograde Cipher."
			);
		}
	}
}

/// Initialization
function init()
{	if ( interval == null ) return;

	// Get DOM nodes or bail if still loading the page.
	txt = document.getElementById( 'txt' );
	key = document.getElementById( 'key' );
	chk = document.getElementById( 'chk' );
	vec = document.getElementById( 'vec' );
	bar = document.getElementById( 'bar' );
	if ( txt == null || key == null || chk == null || vec == null || bar == null ) return;

	// Stop polling
	window.clearInterval( interval );
	interval = null;

	cipher = new RetroCipher();
	if ( chk.checked ) swap();
	document.addEventListener( "mousemove", mouseVec );
	document.addEventListener( "keyup", keyVec );
	document.addEventListener( "keydown", keyVec );

	// Built in pseudo random number generator is weak, but provides some entropy.
	for ( var i = ivLen << 1; --i >= 0; ) addIV( Math.random() * 65536 );
	var n = +(new Date()) ;
	addIV( n );
	addIV( n >> 4 );
	addIV( n >> 8 );
	addIV( n >> 12 );
	addIV( n >> 16 );
	addIV( n >> 18 );
	addIV( n >> 20 );
	addIV( n >> 24 );
	addIV( n >> 28 );
	progress( 16 );
}

// Poll the initialization function 4 times a second until loaded.
interval = window.setInterval( init, 250 );
