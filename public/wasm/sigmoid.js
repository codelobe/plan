/// Copyright (c) 2023 Timothy Landers
/// All rights reserved.  Redistribution and/or modification hereof explicitly prohibited.

//>>> #need libmercury.js
version = 14;

function intToString(n){
  var str="";
  str += typeof n;
  if (typeof n === "number")
  { // 32bit
    str = n.toString(16);
    while (str.length<8) str="0"+str;
    str=" 0x"+str+" ("+(n|0).toString()+")";
  } else {
    // assume 64bit: n[L,H]
    str = int64toHex(n)+" ("+int64toDec(n)+")";
  }
  return str;
}

var ele = null;

// TODO: Remove log() to stand alone library.
var logCount = 0;
function log(txt){
  var s=document.createElement("span");
  s.appendChild(
    document.createTextNode(txt)
  );
  ele.log.appendChild(s);
  logCount++;
}

function clearLog(){
  /*while (logCount--){
      ele.log.remove(ele.log.lastChild);
  }
  */
  logCount = 0;
  var parent=ele.log.parentElement;
  //parent.remove(ele.log);
  ele.log.style.display = "none";
  var lg = document.createElement("div");
  lg.className = "log";
  ele.log=lg;
  parent.appendChild(lg);


}

(()=>{ // faux Sigmoid namespace encapsulation

// Display popup dialog on error.
// TODO remove commented out code.
/*
onerror = function( err ){
    alert(
        (	err.toString()
            + ( err.stack ? '\n' + err.stack.replace( ex.toString() + '\n', '' ) : "" )
        ).replace( /[\s\t\n]+$/, '' )
    );
};
*/

// Set to "done" when complete; is positive while loading; is negative before loading.
var loading = -1;

// Element ID to DOM node table; initialized from idList
var el = {};
ele=el; //todo:remove

// Space delimited list of ID tags that populate the 'el' table upon init() call.
var idList = (
	"moiDisplay showSigmoid showGmoid userSigmoid fun log clearLog testLog logContainer"
    ).split(' ');

// Finds DOM elements for el when body is loaded.
function loader() {
	var body = document.getElementsByTagName("body")[0];
	if ( !body ) {
		loading = setTimeout( loader, 10 );
		return;
	}
	var index;
	for ( index = idList.length; --index >= 0; ){
		var element = document.getElementById( idList[ index ] );
		if ( element ) el[ idList[ index ] ] = element;
		else {
			// Elements are not loaded yet, try again later.
            loading = setTimeout( loader, 20 );
			return;
		}
	}
	clearTimeout( loading );
	loading = "done";
	init();
}
loader();

// Called when finshed loading DOM elements
function init() {
    onerror=function(e){log('\nerr:'+e+'\n');};

   	el.showSigmoid.addEventListener( "click", displaySigmoid, true );
	el.showGmoid.addEventListener( "click", displayGmoid, true );
    el.userSigmoid.addEventListener( "click", displayUser, true );
    //el.fun.value = Sigmoid.toString();
    el.fun.removeAttribute( "readonly" ); // delete el.userSigmoid.readonly; //<- delete don't work.
  log("sig v"+version+"\n");
  log('lib v'+libVersion+'\n')

  el.testLog.addEventListener('click',(e)=>{log((+new Date())+'\n')},true);

  el.clearLog.addEventListener("click",(e)=>{clearLog()},true);

    // TODO: remove test pattern and call default button.
    var ctx = el.moiDisplay.getContext("2d");
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, 768, 768);
    ctx.fillStyle = "green";
    ctx.fillRect(10, 10, 748, 748); // x,y,w,h
}

// TODO: eval textarea to generate user made sigmoid function, or port to JSfiddle temporarily for quick iterations.
var min = Infinity;
var max = -Infinity;
var Sigmoid = function( x )
{   //return x / 2;
    var factor = 256;
    x = x/255; // [0..1]
    x = (x - 0.5) * 2;  // [-1.0 .. +1.0]
    x *= 6; //[-6, +6]
    var result = (1 / ( 1 - Math.exp( -x ) ));
    if ( min > result ) min = result;
    if ( max < result ) max = result;
    return result;
}

// Classic sigmoid function
function displaySigmoid() {
    var ctx = el.moiDisplay.getContext("2d");
    ctx.fillStyle = "#000";
    ctx.fillRect(0, 0, 768, 768);

    ctx.fillStyle = "#ff9922";

    // TODO: iterate the Sigmoid() for 0-255 values, show graph of (in*3,out*3)
    for ( var index = 0; index < 256; ++index)
    {
        var output = Sigmoid( index );
        log("Y="+index+", X="+output+"\n");
        var x = index * 3;
        var h = output * 3;
        var y = 256*3 - h;
        ctx.fillRect( x-.5, y-.5, 3, h ); // 3 here is width of one sample/bar/rect
    }

}

function displayGmoid() {
    var ctx = el.moiDisplay.getContext("2d");
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, 768, 768);

    log( "Min:"+min+"\nMax:"+max+"\nfn() Not Yet Supported!\n" );
}

var first = 1;
function displayUser() {
    var ctx = el.moiDisplay.getContext("2d");
    if ( first ){
		ctx.fillStyle = "#000";
		ctx.globalAlpha = 1;
		ctx.fillRect( 0, 0, 768, 768 );
		ctx.fillStyle = "#ffaa55";
	}	else ctx.fillStyle = "#ff55ff";

    var userSigmoid;
    userSigmoid = eval( "userSigmoid = " + el.fun.value );

    // iterate the Function for 0-255 values.
    for ( var index = 0; index < 256; ++index)
    {   var output = userSigmoid( index );
        var x = index * 3;
        var h = output * 3;
        var y = 768 - h - 1;
        //if ( first | index & 1 )
        ctx.fillRect( x, y - 1, 3, h ); // 3 here is width of one sample/bar/rect
    }

	ctx.rotate( Math.PI );
	ctx.translate( -768, -768 );
	ctx.globalAlpha = .5;
	ctx.globalCompositeOperation = "lighter";
    first = 0;
}

})(); // end Namespace & invoke it.
